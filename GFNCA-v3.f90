      USE hilbert
      implicit none
      character*50 FN,auxheader
      real*8 Dinf,Dsup,dw,t0,t,e0,U,beta,kBT,dGb,conv,wmin,wmax,s,eta,dGs,dGd,mix,Zf,pi,lambda0,normb,norms,normd,deltax,c1,c2,xinf1,xsup1,xinf2,xsup2,ff,eom
      real*8 difference,difftot,diffb,diffs,diffd,denomtot,denomb,denoms,denomd,dSigb,dSigs,dSigd
      real*8 cr,ci,dcr,dci,determinant,d2,d1,v1,v2,xaux,dSigst,E1
      complex*16 ds,cs
      integer i,j,k,Nw,Nval,countb,iwmin,iwmax,lmin,lmax,counts,countd,isign,izero,ixinf1,ixsup1,ixinf2,ixsup2
      real*8,allocatable,dimension(:):: w,nF,Af,x,om,omaux2,dhdx
      complex*16,allocatable,dimension(:):: Deltam1,Deltam1aux1,Deltam1aux2,funcb,funcs,funcd
      real*8,allocatable,dimension(:,:):: ReSigb,ReSigs,ReSigd,ImSigb,ImSigbn,ImSigs,ImSigsn,ImSigd,ImSigdn,Ab,As,Asaux1,Asaux2,Ad
      complex*16 z,zaux1,zaux2
      complex*16,allocatable,dimension(:,:)::Gb,Sigsc,Gd
      Nw=5000
      Nval=1
      eta=1.0d-5
      mix=0.5d0
      pi=dacos(-1.0d0)
      Dinf=-20.0d0
      Dsup=20.0d0
      deltax=(Dsup-Dinf)/(2*Nw)
      wmin=-20.0d0
      wmax=20.0d0
      iwmin = NINT( (wmin-Dinf)/deltax - Nw )
      iwmax = NINT( (wmax-Dinf)/deltax - Nw )
!      print*,'teste com exponencial = ',dexp(2860.0d0)
!      print*,'iwmin=',iwmin, (wmin-Dinf)/deltax - Nw
!      print*,'iwmax=',iwmax, (wmax-Dinf)/deltax - Nw
      
      s=(Dsup+Dinf)/(2.0d0*deltax)
      lmin = iwmin - Nw  - s       !troquei de +s para - s pois vou usar convolu�ao tamb�m
      lmax = iwmax + Nw  + s
      
      print*,'lmin , lmax =',lmin,lmax
!      pause
      allocate(w(lmin:lmax),nF(lmin:lmax),Deltam1(lmin:lmax),Deltam1aux1(lmin:lmax),Deltam1aux2(lmin:lmax),ImSigb(Nval,lmin:lmax),ImSigs(Nval,lmin:lmax),ImSigd(Nval,lmin:lmax))
      allocate(ReSigb(Nval,lmin:lmax),ReSigs(Nval,lmin:lmax),ReSigd(Nval,lmin:lmax))
      allocate(Gb(Nval,lmin:lmax),Sigsc(Nval,lmin:lmax),Gd(Nval,lmin:lmax))
      allocate(Ab(Nval,lmin:lmax),As(Nval,lmin:lmax),Asaux1(Nval,lmin:lmax),Asaux2(Nval,lmin:lmax),Ad(Nval,lmin:lmax),funcb(lmin:lmax),funcs(lmin:lmax),funcd(lmin:lmax))
      allocate(Af(lmin:lmax),ImSigbn(Nval,lmin:lmax),ImSigsn(Nval,lmin:lmax),ImSigdn(Nval,lmin:lmax))
!      allocate(aux1Sigd(Nval,lmin:lmax),aux2Sigd(Nval,lmin:lmax))
      allocate(om(lmin:lmax),omaux2(lmin:lmax),x(lmin:lmax),dhdx(lmin:lmax))
      
      
      ReSigb=0.0d0
      ReSigs=0.0d0
      ReSigd=0.0d0
      ImSigb=0.0d0
      ImSigs=0.0d0
      ImSigd=0.0d0
      Gb=0.0d0
      Sigsc=0.0d0
      Gd=0.0d0
      Ab=0.0d0
      As=0.0d0
      Ad=0.0d0
      Deltam1=0.0d0
      w=0.0d0
      nF=0.0d0
      
      t0=0.4d0
      t=4.0d0
      e0=-3.0d0
      U=7.0d0
      kBT=0.007d0
      beta=1.0d0/kBT
!      lambda0=-3.02599d0
!      lambda0=-2.97400998855002d0
!      lambda0=0.0d0
!      pause 'p0'
      c1=-0.5d0
      c2=0.5d0
!      c1=+1.7d0
      xinf1=-3.0d0
      ixinf1=NINT( (xinf1-Dinf)/deltax - Nw )
      xsup1=0.0d0
      ixsup1=NINT( (xsup1-Dinf)/deltax - Nw )
      xinf2=0.0d0
      ixinf2=NINT( (xinf2-Dinf)/deltax - Nw )
      xsup2=3.0d0
      ixsup2=NINT( (xsup2-Dinf)/deltax - Nw )
      
      Do 01 i=lmin,lmax,1
      x(i)=Dinf+(i+Nw)*deltax
      omaux2(i)=e0+c1*dexp(x(i))
      if(i.lt.ixinf1.or.i.gt.ixsup2)then
      om(i)=x(i)
      dhdx(i)=1.0d0
!      om(i)=e0+c2*dtan(x(i))
!      dhdx(i)=c2/dcos(x(i))**2
      endif
      if(i.gt.ixinf1.and.i.lt.ixsup1)then
!     om(i)=e0+c1*dtan(x(i))
!      dhdx(i)=c1/dcos(x(i))**2
      om(i)=c1*dexp(x(i))
!      dhdx(i)=1.0d0
!      om(i)=e0+c1*dexp(x(i))
      dhdx(i)=c1*dexp(x(i))
      endif
      if(i.gt.ixinf2.and.i.lt.ixsup2)then
!     om(i)=e0+c1*dtan(x(i))
!      dhdx(i)=c1/dcos(x(i))**2
      om(i)=c2*dexp(x(i))
!      dhdx(i)=1.0d0
!      om(i)=e0+c1*dexp(x(i))
      dhdx(i)=c2*dexp(x(i))
      endif
!      w(i)=Dinf+(i+Nw)*dw
      z=dcmplx(x(i),eta)
      zaux1=dcmplx(om(i),eta)
      zaux2=dcmplx(omaux2(i),eta)
      Deltam1aux1(i)=(t0**2)*(zaux1-zaux1*sqrt(1-4*t**2/zaux1**2))/(2*t**2)
      Deltam1aux2(i)=(t0**2)*(zaux2-zaux2*sqrt(1-4*t**2/zaux2**2))/(2*t**2)
      Deltam1(i)=(t0**2)*(z-z*sqrt(1-4*t**2/z**2))/(2*t**2)
      
!      if(om(i).gt.10*kBT)then
!      nF(i)=0.0d0
!      else
      nF(i)=1.0d0/(dexp(+beta*x(i))+1.0d0)
!      endif
      
 01   Enddo
 
      Do 11 i=lmin,lmax,1
!      Deltam1aux1(i)=dcmplx(0.0d0,-0.07d0)
!      Deltam1aux2(i)=dcmplx(0.0d0,-0.07d0)
!      Deltam1(i)=dcmplx(0.0d0,-0.07d0)
!      ReSigb(1,i)=dreal((t0**2)*(z-z*sqrt(1-4*t**2/z**2))/(2*t**2))
!      ReSigs(1,i)=dreal((t0**2)*(z-z*sqrt(1-4*t**2/z**2))/(2*t**2))
!      ReSigd(1,i)=dreal((t0**2)*(z-z*sqrt(1-4*t**2/z**2))/(2*t**2))

      If(i.lt.lmax.and.i.gt.lmin)then     !Coloquei isso aqui porque precisa de x(i+1) e i=lmax vai passar do range do array. Mas no caso i=lmin ainda to na duvida. Talvez nao seja necessario
      
      call locate(x,lmax-lmin + 1 ,0.0d0-x(i),k)
      k=-lmax+k-1
      if(k.eq.lmax)then
      k=k-1
      endif
      if(x(i).gt.0.0d0) then
      ff=1.0d0/(1.0d0+ dexp( (0.0d0-x(i) )/kBT    ) )/(1.0d0-nF(i) )
      else
      eom=dexp(x(i)/kBT)
      ff=(eom+1.0d0)/(eom+dexp(0.0d0/kBT))
      Endif
      ImSigb(1,i)=+2.0d0*ff*(  dimag(Deltam1(k))+( dimag(Deltam1(k+1)) - dimag(Deltam1(k))  )*(   - x(i)- x(k)  )/(x(k+1)-x(k)) )

      call locate(x,lmax-lmin + 1 ,1.0d0+3.0d0-x(i),k)
!      print*,'Valor de k (antes da renormaliza�ao) = ' , k
      k=-lmax+k-1    !Isso aqui � para mapear uma filosofia em outra. Porque aqui usamos arrays de -N:N mas a subrotina locate usa come�ando do 1 (entao fica (1::2N+1))
!      print*,'Valor de 1.0d0+3.0d0-x(i) e k = ' , 1.0d0+3.0d0-x(i), k
      if(k.eq.lmax)then
      k=k-1
      endif

      if(x(i).gt.0.0d0) then
      ff=1.0d0/(1.0d0+ dexp( (1.0d0+3.0d0-x(i) )/kBT    ) )/(1.0d0-nF(i) )
      else
      eom=dexp(x(i)/kBT)
      ff=(eom+1.0d0)/(eom+dexp(4.0d0/kBT))
      Endif
      ImSigs(1,i)= ff*(dimag(Deltam1(k))+( dimag(Deltam1(k+1)) - dimag(Deltam1(k))  )*(1.0d0+3.0d0-x(i)-x(k))/(x(k+1)-x(k)))  !+ &
                    !(  dimag(Deltam1(i))+( dimag(Deltam1(i+1)) - dimag(Deltam1(i))  )*(x(i)- (1.0d0+3.d0) )/(x(i+1)-x(i)) )

      call locate(x,lmax-lmin + 1 ,x(i)-3.0d0,k)
!      print*,'Valor de k (antes da renormaliza�ao) = ' , k
      If(k.eq.0) then
      k=k+1
      Endif
      k=-lmax+k-1    !Isso aqui � para mapear uma filosofia em outra. Porque aqui usamos arrays de -N:N mas a subrotina locate usa come�ando do 1 (entao fica (1::2N+1))
!      print*,'Valor de x(i)-3.0d0 e k = ' , x(i)-3.0d0, k
      if(k.eq.lmin)then
      k=k+1
      endif

      if(x(i).gt.0.0d0)then
      ff=1.0d0/(1.0d0+ dexp( (3.0d0-x(i) )/kBT    ) )/(1.0d0-nF(i) )
      else
      eom=dexp(x(i)/kBT)
      ff=(eom+1.0d0)/(eom+dexp(3.0d0/kBT))
      Endif
      ImSigs(1,i)=ImSigs(1,i) + ff*( dimag(Deltam1(k))+( dimag(Deltam1(k+1)) - dimag(Deltam1(k))  )*(x(i)-3.0d0-x(k))/(x(k+1)-x(k)) )

      call locate(x,lmax-lmin + 1 ,x(i)-0.0d0,k)
      If(k.eq.0) then
      k=k+1
      Endif
      k=-lmax+k-1
      if(k.eq.lmin)then
      k=k+1
      endif
      
      if(x(i).gt.0.0d0)then
      ff=1.0d0/(1.0d0+ dexp( (0.0d0-x(i) )/kBT    ) )/(1.0d0-nF(i) )
      else
      eom=dexp(x(i)/kBT)
      ff=(eom+1.0d0)/(eom+dexp(0.0d0/kBT))
      Endif
      ImSigd(1,i)=+2.0d0*ff*(  dimag(Deltam1(k))+( dimag(Deltam1(k+1)) - dimag(Deltam1(k))  )*(   x(i)- x(k)  )/(x(k+1)-x(k)) )


      Endif
!      ImSigs(1,i)=dimag((t0**2)*(z-z*sqrt(1-4*t**2/z**2))/(2*t**2))
!      ImSigd(1,i)=ImSigb(1,i)

11    Enddo

      Do i=lmin,lmax,1
      funcb(i)=+ImSigb(1,i)*(1.0d0-nF(i))
      funcs(i)=+ImSigs(1,i)*(1.0d0-nF(i))
      funcd(i)=+ImSigd(1,i)*(1.0d0-nF(i))
      Enddo

      call HLBRThilbert(lmax-lmin+1,funcb)
      call HLBRThilbert(lmax-lmin+1,funcs)
      call HLBRThilbert(lmax-lmin+1,funcd)

      Do i=lmin,lmax,1
      ReSigb(1,i)=dreal(funcb(i))
      ReSigs(1,i)=dreal(funcs(i))        !As ReSig aqui sao sem til. Mas as ImSig � com til
      ReSigd(1,i)=dreal(funcd(i))
      Enddo

      Do i=lmin,lmax,1
      Sigsc(1,i)=dcmplx(  ReSigs(1,i)/(dexp( - beta*x(i))+1) , - ImSigs(1,i)/( dexp( - beta*x(i))+1)    )  !- na parte imaginaria porque segundo o Haule � Sig conjugado
      Enddo

!      Sigc=dcmplx(ReSigs(0)*nF(0),ImSigs(0)*nF(0))
      print*,'Valor de x(0) = ',x(0)
!      pause
      E1=e0
      cs=-E1-Sigsc(1,0)
      ds=(Sigsc(1,1)-Sigsc(1,0))/(x(1) - x (0))
      cr=dreal(cs)
      ci=dimag(cs)
      dcr=1.0d0-dreal(ds)
      dci=-dimag(ds)
      dSigst=(ImSigs(1,1)-ImSigs(1,0))/(x(1) - x(0))
      xaux=ImSigs(1,0)/dSigst
      determinant=xaux*(xaux*dcr*dcr+2.0d0*ci*dci)-ci*ci
      if(determinant.le.0.0d0)then
      lambda0=dcr*xaux-cr
      endif
      d2=-sqrt(determinant)
      d1=-cr+dcr*xaux
      v1=1.0d0/(ci**2+(cr+d1+d2)**2)
      v2=1.0d0/(ci**2+(cr+d1-d2)**2)
      if (dabs(v1).gt.dabs(v2))then
      lambda0= d1+d2;
      else
      lambda0=d1-d2;
      Endif
      
      print*,'Valor de lambda0 = ',lambda0
!      pause
      lambda0=-3.02599d0
      
      Do 12 i=lmin,lmax,1
      
      Gb(1,i)=1/(z-dcmplx(   ReSigb(1,i)/(dexp( - beta*x(i))+1)  ,   ImSigb(1,i)/(dexp( - beta*x(i))+1)   ))
!      Gs(1,i)=1/(z-e0-dcmplx(  ReSigs(1,i)/(dexp( - beta*x(i))+1)  ,ImSigs(1,i)/(dexp( - beta*x(i))+1)  ))
      Gd(1,i)=1/(z-2*e0-U-dcmplx(  ReSigd(1,i)/(dexp( - beta*x(i))+1) ,  ImSigd(1,i)/(dexp( - beta*x(i))+1)  ))
!As fun�oes espectrais aqui estao sem o fator 1/pi porque estou seguindo o que o Haule faz
!As fun�oes espectrais aqui sao com til
      Ab(1,i)=(-1.0d0*pi/pi)* ImSigb(1,i)/( ( x(i)+lambda0-ReSigb(1,i) )**2 + ( ImSigb(1,i)/(dexp( - beta*x(i))+1) )**2  )
      As(1,i)=(-1.0d0*pi/pi)* ImSigs(1,i)/( ( x(i)+lambda0-e0-ReSigs(1,i) )**2 + (ImSigs(1,i)/(dexp( - beta*x(i))+1))**2  )
      Asaux1(1,i)=(-1.0d0*pi/pi)* ImSigs(1,i)/( ( x(i)+lambda0-e0-ReSigs(1,i) )**2 + (ImSigs(1,i)/(dexp( - beta*x(i))+1))**2  )
      Asaux2(1,i)=(-1.0d0*pi/pi)* ImSigs(1,i)/( ( om(i)+lambda0-e0-ReSigs(1,i) )**2 + (ImSigs(1,i)/(dexp( - beta*om(i))+1))**2  )
      
      Ad(1,i)=(-1.0d0*pi/pi)* ImSigd(1,i)/( ( x(i)+lambda0-2*e0-U-ReSigd(1,i) )**2 + (ImSigd(1,i)/(dexp( - beta*x(i))+1))**2  )
!      print*,dexp(-beta*x(i))
!      Gb(1,i)=1/(z-0.0d0*dcmplx(   ReSigb(1,i)/(dexp( - beta*x(i))+1)  ,   ImSigb(1,i)/(dexp( - beta*x(i))+1)   ) )
!      Gs(1,i)=1/(z-e0-0.0d0*dcmplx(  ReSigs(1,i)/(dexp( - beta*x(i))+1)  ,ImSigs(1,i)/(dexp( - beta*x(i))+1)  ))
!      Gd(1,i)=1/(z-2*e0-U-0.0d0*dcmplx(  ReSigd(1,i)/(dexp( - beta*x(i))+1) ,  ImSigd(1,i)/(dexp( - beta*x(i))+1)  ))

!      Ab(1,i)= (-1.0d0/pi)*dimag(Gb(1,i))
!      As(1,i)= (-1.0d0/pi)*dimag(Gs(1,i))
!      Ad(1,i)= (-1.0d0/pi)*dimag(Gd(1,i))
      
 12   Enddo
 
OPEN(UNIT=390,file='As-4grids.txt',status='unknown')
WRITE(390,'(6(1x,a25),1(1x,a6))') 'x(j)', 'om(j)', 'As(1,j)', 'Asaux2(j)'
    DO j=lmin,lmax
        WRITE(390,'(6(1x,ES25.15E4),1(1x,i6))') x(j), om(j), As(1,j),Asaux2(1,j)
    END DO
close(UNIT=390)
 
 
      call Anorm(normb,Ab,nF,1,kBT,Nw,Dinf,Dsup,lmin,lmax,Nval)
!      call Anorm2(normb,Ab,x,dhdx,ixinf,ixsup,1,kBT,Nw,Dinf,Dsup,lmin,lmax,Nval)
      print*,'norma de Ab = ',normb/pi

      call Anorm(norms,As,nF,1,kBT,Nw,Dinf,Dsup,lmin,lmax,Nval)
      print*,'norma de As = ',norms/pi

      call Anorm(normd,Ad,nF,1,kBT,Nw,Dinf,Dsup,lmin,lmax,Nval)
      print*,'norma de Ad = ',normd/pi
      pause

!OPEN(UNIT=390,file='output_functions-j=  1.txt',status='unknown')
!WRITE(390,'(13(1x,a12),1(1x,a6))') 'w(j)', 'ReSigb(j)', 'ImSigb(j)','ReSigs(j)', 'ImSigs(j)', 'ReSigd(j)', 'ImSigd(j)', 'ReGb(j)', 'ImGb(j)', 'ReGs(j)', 'ImGs(j)', 'ReGd(j)', 'ImGd(j)','j'
!READ(390,*) auxheader
    DO i=lmin,lmax
!        READ(390,*) x(i), om(i), ReSigb(1,i), ImSigb(1,i), ReSigs(1,i),ImSigs(1,i), ReSigd(1,i),ImSigd(1,i), Ab(1,i), As(1,i), Ad(1,i)
    END DO
!close(UNIT=390)

print*,'Leu as funcoes de input'

OPEN(UNIT=390,file='input_functions.txt',status='unknown')
WRITE(390,'(30(1x,a25),1(1x,a6))') 'x(j)', 'om(j)', 'ReDeltam1(j)', 'ImDeltam1(j)', 'ReSigb(j)', 'ImSigb(j)', 'ReSigs(j)', 'ImSigs(j)','ReSigd(j)', 'ImSigd(j)' , 'Abtilde(j)', 'Astilde(j)', 'Adtilde(j)','Ab(j)', 'As(j)', 'Ad','Asaux1(j)'       !,'Ab(j)', 'As(j)', 'Ad(j)','-Im[Gb]/pi','-Im[Gs]/pi','-Im[Gd]/pi','nF(j)'
    DO j=lmin,lmax
        WRITE(390,'(30(1x,ES25.15E4),1(1x,i6))') x(j), om(j), Deltam1(j), ReSigb(1,j),ImSigb(1,j), ReSigs(1,j),ImSigs(1,j) , ReSigd(1,j),ImSigd(1,j) ,Ab(1,j), As(1,j), Ad(1,j),Ab(1,j)*(1-nF(j)), As(1,j)*(1-nF(j)), Ad(1,j)*(1-nF(j)),Asaux1(1,j)                                !,Ab(1,j)/(dexp( - beta*w(j))+1),As(1,j)/(dexp( - beta*w(j))+1),Ad(1,j)/(dexp( - beta*w(j))+1),-dimag(Gb(1,j))/pi,-dimag(Gs(1,j))/pi,-dimag(Gd(1,j))/pi,nF(j)
    END DO
close(UNIT=390)

print*, 'Escreveu as funcoes de input no arquivo input_functions.txt'

!      pause

      j=2
      conv=1d-4
      countb=10
      counts=10
      countd=10

!      GOTO 135

      Do 2 while ((countb+counts+countd).ne.0)
!      pause 'p1'



      call intsigb(ImSigbn,beta,Deltam1,As,nF,Nw,Dinf,Dsup,deltax,wmin,wmax,iwmin,iwmax,s,lmin,lmax,Nval,1)
      call intsigs(ImSigsn,beta,Deltam1,Ab,Ad,nF,Nw,Dinf,Dsup,deltax,wmin,wmax,iwmin,iwmax,s,lmin,lmax,Nval,1)
      call intsigd(ImSigdn,beta,Deltam1,As,nF,Nw,Dinf,Dsup,deltax,wmin,wmax,iwmin,iwmax,s,lmin,lmax,Nval,1)

!write(FN,'(A19,I3,A4)') 'output_functions-j=',j,'.txt'
OPEN(UNIT=39,file='ImSigbn-ImSigsn-ImSigdn.txt',status='unknown')
WRITE(39,'(30(1x,a25),1(1x,a6))') 'x(j)', 'om(j)', 'ImSigb(j)','ImSigs(j)' ,'ImSigd(j)'
    DO i=lmin,lmax
        WRITE(39,'(30(1x,ES25.15E4),1(1x,i6))') x(i), om(i), ImSigbn(1,i),ImSigsn(1,i),ImSigdn(1,i)
    END DO
close(UNIT=39)
print*,'Escreveu as ImSig da primeira iteracao'

      countb=0
      counts=0
      countd=0
      diffb=0.0d0
      diffs=0.0d0
      diffd=0.0d0
      denomb=0.0d0
      denoms=0.0d0
      denomd=0.0d0
      difference=0.0d0
!      pause 'p1'
      
      Do 03 i=lmin,lmax,1

      ImSigb(1,i)=(1.0d0-mix)*ImSigb(1,i)+mix*ImSigbn(1,i)
      dGb=dabs(ImSigb(1,i)-ImSigbn(1,i))/dabs(ImSigbn(1,i))
      dSigb=dabs(ImSigb(1,i)-ImSigbn(1,i))
      diffb=diffb+dSigb
      denomb=denomb+dabs(ImSigb(1,i))
      if(dGb.gt.conv)then
      countb=countb+1
      endif
      
      ImSigs(1,i)=(1.0d0-mix)*ImSigs(1,i)+mix*ImSigsn(1,i)
      dGs=dabs(ImSigs(1,i)-ImSigsn(1,i))/dabs(ImSigsn(1,i))
      dSigs=dabs(ImSigs(1,i)-ImSigsn(1,i))
      diffs=diffs+dSigs
      denoms=denoms+dabs(ImSigs(1,i))
      if(dGs.gt.conv)then
      counts=counts+1
      endif

      ImSigd(1,i)=(1.0d0-mix)*ImSigd(1,i)+mix*ImSigdn(1,i)
      dGd=dabs(ImSigd(1,i)-ImSigdn(1,i))/ImSigdn(1,i)
      dSigd=dabs(ImSigd(1,i)-ImSigdn(1,i))
      diffd=diffd+dSigd
      denomd=denomd+dabs(ImSigd(1,i))
      if(dGd.gt.conv)then
      countd=countd+1
      endif
  03  Enddo
  
      difftot=diffb+diffs+diffd
      denomtot=denomb+denoms+denomd
      difference=difftot/denomtot
  
      Do i=lmin,lmax,1
      funcb(i)=+ImSigb(1,i)*(1.0d0-nF(i))
      funcs(i)=+ImSigs(1,i)*(1.0d0-nF(i))
      funcd(i)=+ImSigd(1,i)*(1.0d0-nF(i))
      Enddo

      call HLBRThilbert(lmax-lmin+1,funcb)
      call HLBRThilbert(lmax-lmin+1,funcs)
      call HLBRThilbert(lmax-lmin+1,funcd)

      Do i=lmin,lmax,1
      ReSigb(1,i)=dreal(funcb(i))
      ReSigs(1,i)=dreal(funcs(i))
      ReSigd(1,i)=dreal(funcd(i))
      Enddo
  
      Do i=lmin,lmax,1
      Ab(1,i)=(-1.0d0*pi/pi)* ImSigb(1,i)/( ( x(i)+lambda0-ReSigb(1,i) )**2 + (ImSigb(1,i)/(dexp( - beta*x(i))+1))**2  )
      As(1,i)=(-1.0d0*pi/pi)* ImSigs(1,i)/( ( x(i)+lambda0-e0-ReSigs(1,i) )**2 + (ImSigs(1,i)/(dexp( - beta*x(i))+1))**2  )
      Asaux1(1,i)=(1.0d0-mix)*Asaux1(1,i)+mix*(-1.0d0*pi/pi)* ImSigs(1,i)/( ( x(i)+lambda0-e0-ReSigs(1,i) )**2 + (ImSigs(1,i)/(dexp( - beta*x(i))+1))**2  )
      Asaux2(1,i)=(1.0d0-mix)*Asaux2(1,i)+mix*(-1.0d0*pi/pi)* ImSigs(1,i)/( ( omaux2(i)+lambda0-e0-ReSigs(1,i) )**2 + (ImSigs(1,i)/(dexp( - beta*omaux2(i))+1))**2  )
      Ad(1,i)=(-1.0d0*pi/pi)* ImSigd(1,i)/( ( x(i)+lambda0-2*e0-U-ReSigd(1,i) )**2 + (ImSigd(1,i)/(dexp( - beta*x(i))+1))**2  )
      Enddo
      
      print*,'countb',countb
      print*,'counts',counts
      print*,'countd',countd
      print*,'Difference seguindo Haule',difference
      print*,"   "
!Norm of spectral functions

      call Anorm(normb,Ab,nF,1,kBT,Nw,Dinf,Dsup,lmin,lmax,Nval)
      print*,'norma de Ab = ',normb/pi

      call Anorm(norms,As,nF,1,kBT,Nw,Dinf,Dsup,lmin,lmax,Nval)
      print*,'norma de As = ',norms/pi

      call Anorm(normd,Ad,nF,1,kBT,Nw,Dinf,Dsup,lmin,lmax,Nval)
      print*,'norma de Ad = ',normd/pi

write(FN,'(A19,I3,A4)') 'output_functions-j=',j,'.txt'
  
OPEN(UNIT=390,file=FN,status='unknown')
WRITE(390,'(11(1x,a25),1(1x,a6))') 'w(j)', 'om(j)', 'ReSigb(j)', 'ImSigb(j)','ReSigs(j)', 'ImSigs(j)', 'ReSigd(j)', 'ImSigd(j)', 'Ab(j)', 'As(j)', 'Ad(j)','j'
    DO i=lmin,lmax
        WRITE(390,'(11(1x,E25.15E3),1(1x,i6))') x(i), om(i), ReSigb(1,i),ImSigb(1,i), ReSigs(1,i), ImSigs(1,i), ReSigd(1,i),  ImSigd(1,i), Ab(1,i), As(1,i), Ad(1,i),j
    END DO
close(UNIT=390)

      j=j+1
      print*,'Iteracao',j

 2    Enddo

 135  continue
!      call intZf(Zf,1,Ab,As,Ad,kBT,Nw,Dinf,Dsup,lmin,lmax,Nval)
      call intZf2(Zf,1,Ab,As,Ad,nF,kBT,Nw,Dinf,Dsup,lmin,lmax,ixinf1,ixsup1,dhdx,Nval)
      print*,'Zf',Zf
      pause

      call intAf2(Af,kBT,Zf,nF,x,Ab,Ad,Asaux1,Asaux2,Nw,Dinf,Dsup,deltax,wmin,wmax,iwmin,iwmax,s,lmin,lmax,ixinf1,ixsup1,dhdx,Nval,1)
!      pause

OPEN(UNIT=390,file='Spectralfunction.txt',status='unknown')
WRITE(390,'(5(1x,a25),1(1x,a6))') 'w(j)', 'nF(-w)','Af(j)', 'f(-w)*Af(j)','Im[G]'
    DO i=-Nw,Nw
        WRITE(390,'(5(1x,e25.15),1(1x,i6))') x(i),1/(dexp(-beta*x(i))+1),Af(i), Af(i)/(dexp(-beta*x(i))+1),-pi*Af(i)
    END DO
close(UNIT=390)


      stop
      end
      
      include 'intsigb.f90'
      include 'intsigs.f90'
      include 'intsigd.f90'
      include 'intsigb2.f90'
      include 'intsigs2.f90'
      include 'intsigd2.f90'
      include 'integralZf.f'
      include 'integralZf2.f90'
      include 'intAf.f90'
      include 'intAf2.f90'
      include 'Anorm.f'
      include 'Anorm2.f90'
      include 'locate.f'
!      include 'hilbert.f90'
!      call intrhobs(

!===================================================================================================================
!      OPEN(UNIT=390,file='funcb_funcs_funcd.txt',status='unknown')
!WRITE(390,'(7(1x,a25))') 'w', 'funcb', 'Imfuncb=0', 'funcs','Imfuncs=0', 'funcd','Imfuncd=0'
!    DO i=lmin,lmax
!        WRITE(390,'(7(1x,e25.15))') w(i), funcb(i),funcs(i),funcd(i)
!    END DO
!close(UNIT=390)
!      pause 'Escreveu funcb,funcs,funcd no arquivo'

!      ImSigb(1,i)=dimag((t0**2)*(z-z*sqrt(1-4*t**2/z**2))/(2*t**2))
!      ImSigs(1,i)=dimag((t0**2)*(z-z*sqrt(1-4*t**2/z**2))/(2*t**2))
!      ImSigd(1,i)=ImSigb(1,i)

!call intsigb2(ImSigb,beta,x,Deltam1aux1,Deltam1aux2,As,Nw,Dinf,Dsup,deltax,wmin,wmax,iwmin,iwmax,s,lmin,lmax,ixinf,ixsup,dhdx,Nval,Nval)
!call intsigs2(ImSigs,beta,x,Deltam1aux1,Deltam1aux2,Ab,Ad,Nw,Dinf,Dsup,deltax,wmin,wmax,iwmin,iwmax,s,lmin,lmax,ixinf,ixsup,dhdx,Nval,1)
!call intsigd2(aux1Sigd,beta,x,Deltam1aux1,Deltam1aux2,As,Nw,Dinf,Dsup,deltax,wmin,wmax,iwmin,iwmax,s,lmin,lmax,ixinf,ixsup,dhdx,Nval,1)
!call intsigd2(aux2Sigd,beta,x,Deltam1aux1,Deltam1aux2,As,Nw,Dinf,Dsup,deltax,wmin,wmax,iwmin,iwmax,s,lmin,lmax,ixinf,ixsup,dhdx,Nval,1)


!      isign=-1   !convolu�ao
!      izero=1    !(1-nF)

!      isign=+1   !correla�ao
!      izero=0    !nF
!      call intsigs(aux2Sigs,nF,Deltam1,Gd,Nw,Dinf,Dsup,dw,wmin,wmax,iwmin,iwmax,s,lmin,lmax,Nval,j,isign,izero)
!      Sigs=aux1Sigs+aux2Sigs

!      isign=+1   !correla�ao
!      izero=0    !nF
!      call intsigs(aux2Sigs,nF,Deltam1,Gd,Nw,Dinf,Dsup,dw,wmin,wmax,iwmin,iwmax,s,lmin,lmax,Nval,j,isign,izero)
!      Sigs=aux1Sigs+aux2Sigs
