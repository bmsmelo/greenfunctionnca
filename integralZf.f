subroutine intZf(Zf,j2,Ab,As,Ad,kBT2,n2,Dinf2,Dsup2,lmin,lmax,Nval)

! Integration of f(x) on [a,b]
! Method: Simpson rule for n intervals
! written by: Alex Godunov (October 2009)
!----------------------------------------------------------
! IN:
! f   - Function to integrate (supplied by a user)
! a	  - Lower limit of integration
! b	  - Upper limit of integration
! n   - number of intervals
! OUT:
! integral - Result of integration
!==========================================================
      implicit none
      integer lmin,lmax,Nval
!      parameter (nphys=10001)
!      parameter (Nval=20)
      double precision f2, Dsup2, Dinf2, integral2,s
      double precision kBT2, Zf
      double precision h, omega2
      complex*16 Ab(Nval,lmin:lmax),As(Nval,lmin:lmax),Ad(Nval,lmin:lmax)
!      integer index2
      integer n2, i2 , j2
!      integer nint
! if n is odd we add +1 to make it even
      if((n2/2)*2.ne.n2) n2=n2+1

! loop over n (number of intervals)
      s = 0.0d0
      h = (Dsup2-Dinf2)/(2.0d0*dfloat(n2))

!      do i2=5, n2-5, 2
       do i2=-n2+5,n2-5,2
!         omega2   = Dinf2+dfloat(i2)*h
         omega2   = Dinf2+dfloat(i2+n2)*h

         s = s + 2.0d0*f2(j2,i2,omega2,kBT2,Ab,As,Ad,lmin,lmax,Nval) + 4.0d0*f2(j2,i2+1,omega2+h,kBT2,Ab,As,Ad,lmin,lmax,Nval)
          !to levando o indice da itera��o j2 e o �ndice do omega i2
          !i2 s�o frequencias pares e i2+1 s�o as �mpares
      end do

!      print*,'Aharony-soma',s
!      pause

      integral2 =(s/3.0d0 + 27.0d0*f2(j2,-n2+1,Dinf2+h,kBT2,Ab,As,Ad,lmin,lmax,Nval)/12.0d0      &
                          + 13.0d0*f2(j2,-n2+3,Dinf2+3*h,kBT2,Ab,As,Ad,lmin,lmax,Nval)/12.0d0    &
                          + 27.0d0*f2(j2,n2-1,Dsup2-h,kBT2,Ab,As,Ad,lmin,lmax,Nval)/12.0d0   &
                          + 13.0d0*f2(j2,n2-3,Dsup2-3*h,kBT2,Ab,As,Ad,lmin,lmax,Nval)/12.0d0 &
                          + 4.0d0*f2(j2,-n2+4,Dinf2+4*h,kBT2,Ab,As,Ad,lmin,lmax,Nval)/3.0d0)*h
      Zf=integral2
!      print*,'integral2',integral2
!      print*,'Aharony',
!     -27.0d0*f2(j2,1,Dinf2+h,kBT2,G2)/12.0d0,
!     - 13.0d0*f2(j2,3,Dinf2+3*h,kBT2,G2)/12.0d0,
!     - 27.0d0*f2(j2,n2-1,Dsup2-h,kBT2,G2)/12.0d0,
!     - 13.0d0*f2(j2,n2-3,Dsup2-3*h,kBT2,G2)/12.0d0,
!     -4.0d0*f2(j2,4,Dinf2+4*h,kBT2,G2)/3.0d0

!      pause
!      return
      end subroutine intZf

!=======================================================================
      function f2(j,i,w,kBT,Ab,As,Ad,lmin,lmax,Nval)
      implicit none
      integer j,i , lmin,lmax ,Nval
!      parameter (nphys=100001)
!      parameter (Nval=20)
      real*8 pi , beta , kBT , w , f2
      real*8 Ab(Nval,lmin:lmax),As(Nval,lmin:lmax),Ad(Nval,lmin:lmax)

      pi=dacos(-1.0d0)
      beta=1/kBT
!      nF=1/(dexp(+beta*w)+1)
!      f2=-dexp(-beta*w)*( dimag(Gb(j,i))/pi + 2*dimag(Gs(j,i))/pi  + dimag(Gd(j,i))/pi )
      if(beta*w.ge.10.0d0)then
      f2=0.0d0
      else
      f2=+(1.0d0/(dexp(+beta*w)+1))*( Ab(j,i) + 2*As(j,i)  + Ad(j,i))
      endif
!      print*,'NCA-exp(beta*w) e f2',i,w,dexp(-beta*w),f2,j
!      pause

!      print*,'Aharony-omega e f2',w,f2
!      pause

!      if(f2.lt.0.0d0)then
!      print*,'f2',f2
!      print*,'G',G(j,i),j,i
!      pause
!      endif

      return
      end
!=======================================================================
