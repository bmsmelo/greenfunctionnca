SUBROUTINE intsigb(y,beta,Hybm,As,nF,N,A,B,Delta,tmin,tmax,jmin,jmax,s,lmin,lmax,Nval,index)

IMPLICIT NONE
INTEGER                         :: N, j, k, kmin, kmax, jmin, jmax ,lmax,lmin,l,Nval,index
DOUBLE PRECISION                :: sum1(lmin:lmax),ti,tf,dt,dtm,pi,y(Nval,lmin:lmax),g(lmin:lmax),As(Nval,lmin:lmax),nF(lmin:lmax) !,convlv
DOUBLE COMPLEX                  :: Hybm(lmin:lmax)
DOUBLE PRECISION , ALLOCATABLE   :: t(:)
DOUBLE PRECISION                :: A, B, Delta, tmin, tmax,s ,beta

ALLOCATE( t(lmin:lmax) )
pi=dacos(-1.0d0)
!g=nF*Hybm
open(3000,file='t_g_h.txt',status='unknown')
DO k=lmin,lmax
    t(k) = A  + (k+N)*Delta
!    lambda(k) = t(k)
    g(k) = ( - dimag(Hybm(k)))  !*nF(k) ! De acordo com o paper dos argentinos Deltam � -Im[Gkm].
                                     !E no paper do Hetler ele chama de N(omega) uma densidade de estados, que � -Im[Gkm] tbm
!    h(k)=As(index,k)*(1-nF(k))
!    write(3000,'(6(1x,E25.15E4))')t(k),g(k),As(index,k)
END DO
close(3000)

DO j=jmin,jmax
    sum1(j)=0.0d0
         if(t(j).lt.0.0d0)then
         DO k=-N,N-1,1     !N-1
         l=k-j-s
         sum1(j)=sum1(j) + g(l)*As(index,k)*nF(k)/( nF(j)* (  dexp(beta*( t(j) - t(k) ) ) + 1.0d0  )  )
         ENDDO
         Else
         DO k=-N,N-1,1
         l=k-j-s
         sum1(j)=sum1(j) + g(l)*As(index,k)*(1-nF(k))/( (1-nF(j))* (dexp(beta*( t(k) - t(j) ) ) + 1.0d0)  )
         ENDDO
         Endif
    y(index,j) = - 2.0d0*Delta*(sum1(j))/pi    ! fator 2 vem da degeneresc�ncia na soma em m
END DO

RETURN
END SUBROUTINE intsigb

!==============================================================================
!Lixo
!N = 10000 ! 2N+1 points in the grid
!A=   -10.d0
!B=    15.d0
!tmin = -5.d0
!tmax =  5.d0


!Delta = (B-A)/(2.d0*N)
!jmin = NINT( (tmin-A)/Delta - N )
!jmax = NINT( (tmax-A)/Delta - N )
!s=(A+B)/(2.d0*Delta)
!lmin = jmin - N  + s
!lmax = jmax + N  + s

!ALLOCATE( y(kmin:kmax), g(kmin:kmax), h(kmin:kmax), f(kmin:kmax,kmin:kmax), lambda(kmin:kmax), t(kmin:kmax) )
!ALLOCATE( y(lmin:lmax), g(lmin:lmax), h(lmin:lmax), f(jmin:jmax,lmin:lmax), lambda(lmin:lmax), t(lmin:lmax) )

!h = 0.d0
!g = 0.d0
!y = 0.d0

    ! h(t)
!    IF (t(k).GE.-1.d0) THEN
!        IF (t(k).LE.4.d0) THEN
!            h(k) = 2.d0
!        END IF
!    END IF
    ! g(lambda)
!    IF (lambda(k).GE.0.d0) THEN
!        IF (lambda(k).LE.3.d0) THEN
!            g(k) = 3.d0
!        END IF
!    END IF

!PRINT*, 'Delta,jmin_int,jmin,jmax_int,jmax',Delta,jmin,(tmin-A)/Delta - N ,jmax ,(tmax-A)/Delta - N
!PRINT*, 'tmin,tmax',tmin, tmax
!PRINT*, 't(jmin),t(jmax)',t(jmin), t(jmax)
!PRINT*, 'lmin,lmax',lmin, lmax
!PRINT*, 'lambda(lmin),lambda(lmax)',lambda(lmin), lambda(lmax)
!PRINT*, 't(-N),t(N)',t(-N), t(N)


!OPEN(UNIT=390,file='correlation_v04.txt',status='unknown')
!WRITE(390,'(5(1x,a12),1(1x,a6))') 't(j)', 'lambda(j)', 'g(j)', 'h(j)', 'y(j)', 'j'
!    DO j=-N,N
!        WRITE(390,'(5(1x,e12.5),1(1x,i6))') t(j), lambda(j), g(j), h(j), y(j), j
!    END DO
!CLOSE(UNIT=390)



!OPEN(UNIT=390,file='f.txt',status='unknown')
!    DO j=jmin,jmax
!        DO k=-N,N-1
!!            WRITE(390,'(2(1x,e12.5),1(1x,e12.5),1(1x,i6))') t(j), lambda(k), f(j,k)
!        END DO
!    END DO
!CLOSE(UNIT=390)

!STOP

!ImSigb,beta,Deltam1,As,Nw,Dinf,Dsup,dw,wmin,wmax,iwmin,iwmax,s,lmin,lmax,Nval,1
!PROGRAM main

!        f(j,k) = g(k)*h(iter-1,l)

 !       l=j+k+s

!         Endif
!        sum = sum + g(k)*As(index,k)*( 1.0d0 + dexp(beta*t(j)) )/(2.0d0+2.0d0*dcosh( beta*(t(k)-t(j) )  ))

!       sum = sum + g(l)*h(k)/(1.0d0-nF(j))! + &
!                  g(l)*As(index,k)*1.0d0/(  ( dexp(beta*t(k) ) + dexp(beta*t(j) ) ) * (dexp(-beta* t(k) ) + 1.0d0 )   )

!       sum=sum+g(l)*As(index,k)*1.0d0/(  (dexp(beta* ( t(k) - t(j) )  ) + 1.0d0 ) * (dexp(-beta* t(k) ) + 1.0d0 )   )+ &
!                 g(l)*As(index,k)*1.0d0/(  (dexp(beta* ( t(k) - t(j) )  ) + 1.0d0 ) * (dexp(-beta* ( t(k) - t(j) ) ) + 1.0d0* dexp(beta*t(j)) )    )
!    print*,k, t(j), beta*t(j), ( 1.0d0 + dexp(beta*t(j)) )/(2.0d0+2.0d0*dcosh( beta*(t(k)-t(j) )  ))
!     print*,k,t(j),beta*t(k),beta*t(j),dexp(  beta* ( t(j)  ) ),dexp(  beta* ( t(k)  ) ),  dexp( - beta* ( t(k)  ) ),1.0d0/(dexp( - beta* ( t(k)  ) )  + 1.0d0),  1.0d0/(  ( dexp(beta*t(k)   ) + 1.0d0* dexp(beta*t(j)) ) )   !1.0d0/(  (dexp(beta* ( t(k) - t(j) )  ) + 1.0d0 ) * (dexp(-beta* t(k) ) + 1.0d0 )   ),1.0d0/( dexp(-beta*t(k) ) + 1.0d0  )* 1.0d0/(  ( dexp(beta*t(k)   ) + 1.0d0* dexp(beta*t(j)) ) )
!    print*,k,t(j),beta*t(k),beta*t(j), dexp(beta*t(k) ),dexp(beta*t(j) ),1.0d0/( dexp(beta*t(k) ) + dexp(beta*t(j) ) ), 1.0d0/(dexp(-beta* t(k) ) + 1.0d0 )
!    print*,k,j,  g(l)*As(index,k)*nF(k)/( nF(j)* (dexp(beta*( t(j) - t(k) ) ) + 1.0d0)  ) , dexp(beta*( t(j) - t(k) ) ) + 1.0d0, nF(j)

!         if(t(j).gt.4.9d0)then
!         print*,k,j,t(j),g(l)*As(index,k)*(1.0d0-nF(k))/( (1.0d0-nF(j))* (dexp(beta*( t(k) - t(j) ) ) + 1.0d0)  ) , dexp(beta*( t(k) - t(j) ) ) , (1.0d0- nF(j))
!         endif



!        sum2(j)=sum2(j)+ g(l)*As(index,k)*(1.0d0-nF(k))*nF(l)/(1.0d0-nF(j))
!        print*,k,j,g(l)*As(index,k)*(1.0d0-nF(k))*nF(l)/(1.0d0-nF(j)),nF(l),(1.0d0-nF(j))

!         print*,k,j,t(j),g(l)*As(index,k)*nF(k)/( nF(j)* (dexp(beta*( t(j) - t(k) ) ) + 1.0d0)  )
