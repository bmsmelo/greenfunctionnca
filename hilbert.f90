       include 'mkl_dfti.f90'
       MODULE hilbert
!
!   Modules
!
         use MKL_DFTI
         implicit none
         PUBLIC :: HLBRThilbert
         PRIVATE :: HLBRTeqKernel
       CONTAINS
!  *******************************************************************
!                             HLBRThilbert
!  *******************************************************************
!  Description: Computes a Hilbert transform H{Sr(w')}(w) on an
!  equidistant grid by discrete convolution with FFT.
!
!  Uses Intel MKL FFT algorithm.
!
!  Written by Pedro Brandimarte, Feb 2013.
!  Instituto de Fisica
!  Universidade de Sao Paulo
!  e-mail: brandimarte@gmail.com
!  ***************************** HISTORY *****************************
!  Original version:    February 2013
!  ****************************** INPUT ******************************
!  integer npts               : # of energy points
!  ************************** INPUT/OUTPUT ***************************
!  complex*8 func(npts)       : function to be evaluated
!  *******************************************************************
         subroutine HLBRThilbert (npts, func)
!   Input variables.
           integer :: npts
           double complex :: func(npts)

!   Local variables.
           integer :: i, e
           real*8 :: foo
           double complex, dimension (:), allocatable :: ker, Aux

!   Intel MKL types.

           TYPE(DFTI_DESCRIPTOR), pointer :: MKLdesc
           integer :: MKLstatus

!   Allocate kernel and auxiliary arrays.
           allocate(ker(2*npts))
           allocate(Aux(2*npts))

!   Compute the Fourier transform of the kernel function.
           call HLBRTeqKernel (npts, ker)

!   Allocate and initialize the descriptor data structure.
           MKLstatus = DftiCreateDescriptor (MKLdesc, DFTI_DOUBLE, &
                DFTI_COMPLEX, 1, 2*npts)

!   Set the scale factor for the backward transform
!   (to make it really the inverse of the forward).
           foo = 1.D0 / (2.D0 * npts)
           MKLstatus = DftiSetValue (MKLdesc, DFTI_BACKWARD_SCALE, foo)

!   Complete initialization of the previously created descriptor.
           MKLstatus = DftiCommitDescriptor (MKLdesc)

           Aux = 0.D0
           Aux(1:npts) = func
           MKLstatus = DftiComputeForward (MKLdesc, Aux)

           do e = 1,2*npts
              Aux(e) = ker(e) * Aux(e)
           enddo

!   Compute the backward FFT.
           MKLstatus = DftiComputeBackward (MKLdesc, Aux)
           func = Aux(1:npts)

!   Free memory.
           deallocate(ker)
           deallocate(Aux)
           MKLstatus = DftiFreeDescriptor (MKLdesc)

         end subroutine HLBRThilbert
!  *******************************************************************
!                             HLBRTeqKernel
!  *******************************************************************
!  Description: Computes the kernel function (and its Fourier
!  transform) associated with a linear interpolation on an equidistant
!  energy grid.
!
!  Uses Intel MKL FFT algorithm.
!
!  Written by Pedro Brandimarte, Feb 2013.
!  Instituto de Fisica
!  Universidade de Sao Paulo
!  e-mail: brandimarte@gmail.com
!  ***************************** HISTORY *****************************
!  Original version:    February 2013
!  ****************************** INPUT ******************************
!  integer npts               : # of energy points
!  ***************************** OUTPUT ******************************
!  complex*8 ker(2*npts)      : kernel function
!  *******************************************************************
         subroutine HLBRTeqKernel (npts, ker)

!   Input variables.
           integer :: npts
           double complex :: ker(2*npts)

!   Local variables.
           integer :: i
           real*8, parameter :: pi = 3.1415926535897932384626433832795028841D0
           real*8, dimension (:), allocatable :: foo

!   Intel MKL types.
           TYPE(DFTI_DESCRIPTOR), pointer :: MKLdesc
           integer :: MKLstatus

!   Allocate auxiliary vector.
           allocate(foo(npts+1))

!   Build the kernel function.
           foo(1) = 0.D0
           do i = 1,npts
              foo(i+1) = 1.D0 * i * DLOG(1.D0 * i)
           enddo

           ker(1) = 0.D0
           ker(npts+1) = 0.D0
           do i = 1,npts-1
              ker(i+1) = DCMPLX(foo(i+2) - 2.D0*foo(i+1) + foo(i))
              ker(2*npts-i+1) = - ker(i+1)
           enddo

!   Allocate and initialize the descriptor data structure.
           MKLstatus = DftiCreateDescriptor (MKLdesc, DFTI_DOUBLE, & 
                DFTI_COMPLEX, 1, 2*npts)

!   Complete initialization of the previously created descriptor.
           MKLstatus = DftiCommitDescriptor (MKLdesc)

!   Compute the forward FFT.
           MKLstatus = DftiComputeForward (MKLdesc, ker)

           ker = - ker / pi

!   Free memory
           MKLstatus = DftiFreeDescriptor (MKLdesc)
           deallocate(foo)

         end subroutine HLBRTeqKernel

!  *******************************************************************

       END MODULE hilbert
