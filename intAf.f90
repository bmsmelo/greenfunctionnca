!Observa�ao: A fun�ao spectral deve ser definida como double precision. Quando define como double complex d� erro de "acess violation"
SUBROUTINE intAf(y,kBT,Zf,nF,Ab,Ad,As,N,A,B,Delta,tmin,tmax,jmin,jmax,s,lmin,lmax,Nval,index)
!PROGRAM main
IMPLICIT NONE
INTEGER                         :: N, j, k, kmin, kmax, jmin, jmax ,lmax,lmin,l0,l1,l2,Nval,index,isign,izero
DOUBLE PRECISION                :: kBT,ti,tf,dt,dtm,pi,beta,Zf,sum0,sum1,y(lmin:lmax) !,convlv
DOUBLE PRECISION                :: Ab(Nval,lmin:lmax),Ad(Nval,lmin:lmax),As(Nval,lmin:lmax)
DOUBLE PRECISION , ALLOCATABLE  :: y0(:),y1(:),g0(:),g1(:)
DOUBLE PRECISION , ALLOCATABLE   :: t(:)
DOUBLE PRECISION                :: A, B, Delta, tmin, tmax,s,nF(lmin:lmax)

ALLOCATE( t(lmin:lmax),y0(lmin:lmax),y1(lmin:lmax),g0(lmin:lmax),g1(lmin:lmax) )
pi=dacos(-1.0d0)
beta=1.0d0/kBT
pause 'Af-p0'
DO k=lmin,lmax
    t(k) = A  + (k+N)*Delta
!    lambda(k) = t(k)
!    g(k) = isign*nF(k)*dimag(Hybm(k)) + izero*dimag(Hybm(k))    !(1.0d0-nF(k))*Hybm(k) . O isign � para corretamente levar em conta o sinal de nF*Hyb
    g0(k) = Ab(index,k)      !Equa��o 11 do paper dos argentinos (2a equa�ao)
    g1(k) = Ad(index,k)
!    g2(k)= nF(k)*dimag(Hybm(k))
END DO
!print*,'jmin,jmax',jmin,jmax
!print*,'lmin,lmax',lmin,lmax
!pause 'Af-p1'
DO j=jmin,jmax,1
!if(t(j).gt.-20d0*kBT.and.t(j).lt.20d0*kBT) then
    sum0 =0.0d0
    sum1=0.0d0
!    sum2=0.0d0
!   print*,'antes de entrar no loop k'
    DO k=-N,N-1,1
        l0=j+k+s
!        l1=j-k-s
        l1=k-j-s  !aqui o argumento � epsilon - omega e nao omega - epsilon na �ltima das equa�oes 11 do paper dos argentinos
!        l2=j+k+s      ! o ising � para levar em conta corretamente se � correla�ao ou convolu�ao
!        f(j,k) = g(k)*h(index,l)
!       if(j.gt.935)then
!        print*,'Valores',j,k,l0,l1,index,g0(k),g1(k),Gs(index,l1)
!       endif
        sum0 = sum0 + g0(k)*As(index,l0)*(dexp( - beta*t(j))+1)/( (dexp(beta* (- t(k) - t(j))) + 1 ) * (dexp(+beta* t(k) ) + 1 )            )
        sum1 = sum1 + g1(k)*As(index,l1)*(dexp( + beta*t(j))+1)/( (dexp(beta* ( - t(k) + t(j))) + 1 ) * (dexp(+beta* t(k) ) + 1 )            )
!        sum2 = sum2 + g2(k)*h2(index,l2)

    END DO
!Zf=1.0d0

    y0(j)= Delta*sum0/Zf

    y1(j) = Delta*sum1/Zf

    y(j) = +(y0(j)+y1(j))
!Endif
END DO
!pause 'Af-p2'
DEALLOCATE(t,y0,y1,g0,g1)
RETURN
END SUBROUTINE intAf

!==============================================================================
!Lixo
!N = 10000 ! 2N+1 points in the grid
!A=   -10.d0
!B=    15.d0
!tmin = -5.d0
!tmax =  5.d0


!Delta = (B-A)/(2.d0*N)
!jmin = NINT( (tmin-A)/Delta - N )
!jmax = NINT( (tmax-A)/Delta - N )
!s=(A+B)/(2.d0*Delta)
!lmin = jmin - N  + s
!lmax = jmax + N  + s

!ALLOCATE( y(kmin:kmax), g(kmin:kmax), h(kmin:kmax), f(kmin:kmax,kmin:kmax), lambda(kmin:kmax), t(kmin:kmax) )
!ALLOCATE( y(lmin:lmax), g(lmin:lmax), h(lmin:lmax), f(jmin:jmax,lmin:lmax), lambda(lmin:lmax), t(lmin:lmax) )

!h = 0.d0
!g = 0.d0
!y = 0.d0

    ! h(t)
!    IF (t(k).GE.-1.d0) THEN
!        IF (t(k).LE.4.d0) THEN
!            h(k) = 2.d0
!        END IF
!    END IF
    ! g(lambda)
!    IF (lambda(k).GE.0.d0) THEN
!        IF (lambda(k).LE.3.d0) THEN
!            g(k) = 3.d0
!        END IF
!    END IF

!PRINT*, 'Delta,jmin_int,jmin,jmax_int,jmax',Delta,jmin,(tmin-A)/Delta - N ,jmax ,(tmax-A)/Delta - N
!PRINT*, 'tmin,tmax',tmin, tmax
!PRINT*, 't(jmin),t(jmax)',t(jmin), t(jmax)
!PRINT*, 'lmin,lmax',lmin, lmax
!PRINT*, 'lambda(lmin),lambda(lmax)',lambda(lmin), lambda(lmax)
!PRINT*, 't(-N),t(N)',t(-N), t(N)


!OPEN(UNIT=390,file='correlation_v04.txt',status='unknown')
!WRITE(390,'(5(1x,a12),1(1x,a6))') 't(j)', 'lambda(j)', 'g(j)', 'h(j)', 'y(j)', 'j'
!    DO j=-N,N
!        WRITE(390,'(5(1x,e12.5),1(1x,i6))') t(j), lambda(j), g(j), h(j), y(j), j
!    END DO
!CLOSE(UNIT=390)



!OPEN(UNIT=390,file='f.txt',status='unknown')
!    DO j=jmin,jmax
!        DO k=-N,N-1
!!            WRITE(390,'(2(1x,e12.5),1(1x,e12.5),1(1x,i6))') t(j), lambda(k), f(j,k)
!        END DO
!    END DO
!CLOSE(UNIT=390)

!STOP
