subroutine intZf2(Zf,j2,Ab,As,Ad,nF,kBT2,n2,Dinf2,Dsup2,lmin,lmax,ixinf,ixsup,dhdx,Nval)

!Zf,j2,Ab,As,Ad,kBT2,n2,Dinf2,Dsup2,lmin,lmax,Nval

implicit none
integer lmin,lmax,Nval,ixinf,ixsup
!      parameter (nphys=10001)
!      parameter (Nval=20)
double precision f22, Dsup2, Dinf2, integral1,integral2,integral3,s1,s2,s3
double precision kBT2, integral ,Zf
double precision deltax, omega2
real*8 Ab(Nval,lmin:lmax),As(Nval,lmin:lmax),Ad(Nval,lmin:lmax),x(lmin:lmax),dhdx(lmin:lmax),nF(lmin:lmax)
!      integer index2
integer n2, i2 , j2

s1=0.0d0
s2=0.0d0
s3=0.0d0
deltax = (Dsup2-Dinf2)/(2.0d0*dfloat(n2))
Do i2=-n2+1,ixinf-1,1
s1=s1+nF(i2)*( Ab(j2,i2) + 2*As(j2,i2)  + Ad(j2,i2))*dhdx(i2)  !1/cos � a derivada dh/dx
Enddo
integral1=(s1+ 0.5d0*nF(-n2)*( Ab(j2,-n2) + 2*As(j2,-n2)  + Ad(j2,-n2))*dhdx(-n2) + 0.5d0*nF(ixinf)*( Ab(j2,ixinf) + 2*As(j2,ixinf)  + Ad(j2,ixinf))*dhdx(ixinf)    )*deltax

Do i2=ixinf+1,ixsup-1,1
s2=s2+nF(i2)*( Ab(j2,i2) + 2*As(j2,i2)  + Ad(j2,i2))*dhdx(i2)
Enddo
integral2=(s2+ 0.5d0*nF(ixinf)*( Ab(j2,ixinf) + 2*As(j2,ixinf)  + Ad(j2,ixinf))*dhdx(ixinf) + 0.5d0*nF(ixsup)*( Ab(j2,ixsup) + 2*As(j2,ixsup)  + Ad(j2,ixsup))*dhdx(ixsup)    )*(+deltax)

Do i2=ixsup+1,n2-1,1
s3=s3+nF(i2)*( Ab(j2,i2) + 2*As(j2,i2)  + Ad(j2,i2))*dhdx(i2)
Enddo
integral3=(s3+ 0.5d0*nF(ixsup)*( Ab(j2,ixsup) + 2*As(j2,ixsup)  + Ad(j2,ixsup))*dhdx(ixsup) + 0.5d0*nF(n2)*( Ab(j2,n2) + 2*As(j2,n2)  + Ad(j2,n2))*dhdx(n2)    )*deltax


Zf=integral1+integral2+integral3

return
End subroutine intZf2
