SUBROUTINE intsigs(y,beta,Hybm,Ab,Ad,nF,N,A,B,Delta,tmin,tmax,jmin,jmax,s,lmin,lmax,Nval,index)
!PROGRAM main
IMPLICIT NONE
INTEGER                         :: N, j, k, kmin, kmax, jmin, jmax ,lmax,lmin,l0,l1,l2,Nval,index,isign,izero
DOUBLE PRECISION                :: ti,tf,dt,dtm,pi !,convlv
DOUBLE PRECISION                :: y(Nval,lmin:lmax),Ab(Nval,lmin:lmax),Ad(Nval,lmin:lmax),sum0(lmin:lmax),sum1(lmin:lmax),nF(lmin:lmax)
DOUBLE COMPLEX                  ::  Hybm(lmin:lmax)
DOUBLE PRECISION , ALLOCATABLE    :: y0(:),y1(:),g0(:),g1(:)
DOUBLE PRECISION , ALLOCATABLE   :: t(:)
DOUBLE PRECISION                :: A, B, Delta, tmin, tmax,s,beta

ALLOCATE( t(lmin:lmax),y0(lmin:lmax),y1(lmin:lmax),g0(lmin:lmax),g1(lmin:lmax) )
pi=dacos(-1.0d0)
DO k=lmin,lmax
    t(k) = A  + (k+N)*Delta
!    lambda(k) = t(k)
!    g(k) = isign*nF(k)*dimag(Hybm(k)) + izero*dimag(Hybm(k))    !(1.0d0-nF(k))*Hybm(k) . O isign � para corretamente levar em conta o sinal de nF*Hyb
    g0(k) = ( - dimag(Hybm(k)))
    g1(k) = ( - dimag(Hybm(k)) )
!    g2(k)= nF(k)*( - dimag(Hybm(k)))
END DO

DO j=jmin,jmax
    sum0(j)=0.0d0
    sum1(j)=0.0d0
    If(t(j).lt.0.0d0)then
    DO k=-N,N-1
        l0=j-k-s
        l1=k-j-s
        sum0(j) = sum0(j) + g0(l0)*Ab(index,k)*nF(k)/( nF(j)* (  dexp(beta*( t(j) - t(k) ) ) + 1.0d0  )  )    !*(dexp( - beta*t(j))+1)/( (dexp(beta* (t(k) - t(j))) + 1 ) * (dexp(-beta* t(k) ) + 1 )            )
        sum1(j) = sum1(j) + g1(l1)*Ad(index,k)*nF(k)/( nF(j)* (  dexp(beta*( t(j) - t(k) ) ) + 1.0d0  )  )    !*(dexp( - beta*t(j))+1)/( (dexp(beta* (t(k) - t(j))) + 1 ) * (dexp(-beta* t(k) ) + 1 )            )
    END DO
    Else
    DO k=-N,N-1,1
        l0=j-k-s
        l1=k-j-s
        sum0(j) = sum0(j) + g0(l0)*Ab(index,k)*(1.0d0-nF(k))/( (1.0d0-nF(j))* (  dexp(beta*( t(k) - t(j) ) ) + 1.0d0  )  )    !*(dexp( - beta*t(j))+1)/( (dexp(beta* (t(k) - t(j))) + 1 ) * (dexp(-beta* t(k) ) + 1 )            )
        sum1(j) = sum1(j) + g1(l1)*Ad(index,k)*(1.0d0-nF(k))/( (1.0d0-nF(j))* (  dexp(beta*( t(k) - t(j) ) ) + 1.0d0  )  )    !*(dexp( - beta*t(j))+1)/( (dexp(beta* (t(k) - t(j))) + 1 ) * (dexp(-beta* t(k) ) + 1 )            )
    END DO
    ENDIF
    
    y0(j)= Delta*sum0(j)
    y1(j) = Delta*sum1(j)
!    y2(j) = Delta*sum2
    y(index,j) = -(y0(j)+y1(j))/pi
END DO
DEALLOCATE(t,y0,y1,g0,g1)
RETURN
END SUBROUTINE intsigs

!==============================================================================
!Lixo
!N = 10000 ! 2N+1 points in the grid
!A=   -10.d0
!B=    15.d0
!tmin = -5.d0
!tmax =  5.d0


!Delta = (B-A)/(2.d0*N)
!jmin = NINT( (tmin-A)/Delta - N )
!jmax = NINT( (tmax-A)/Delta - N )
!s=(A+B)/(2.d0*Delta)
!lmin = jmin - N  + s
!lmax = jmax + N  + s

!ALLOCATE( y(kmin:kmax), g(kmin:kmax), h(kmin:kmax), f(kmin:kmax,kmin:kmax), lambda(kmin:kmax), t(kmin:kmax) )
!ALLOCATE( y(lmin:lmax), g(lmin:lmax), h(lmin:lmax), f(jmin:jmax,lmin:lmax), lambda(lmin:lmax), t(lmin:lmax) )

!h = 0.d0
!g = 0.d0
!y = 0.d0

    ! h(t)
!    IF (t(k).GE.-1.d0) THEN
!        IF (t(k).LE.4.d0) THEN
!            h(k) = 2.d0
!        END IF
!    END IF
    ! g(lambda)
!    IF (lambda(k).GE.0.d0) THEN
!        IF (lambda(k).LE.3.d0) THEN
!            g(k) = 3.d0
!        END IF
!    END IF

!PRINT*, 'Delta,jmin_int,jmin,jmax_int,jmax',Delta,jmin,(tmin-A)/Delta - N ,jmax ,(tmax-A)/Delta - N
!PRINT*, 'tmin,tmax',tmin, tmax
!PRINT*, 't(jmin),t(jmax)',t(jmin), t(jmax)
!PRINT*, 'lmin,lmax',lmin, lmax
!PRINT*, 'lambda(lmin),lambda(lmax)',lambda(lmin), lambda(lmax)
!PRINT*, 't(-N),t(N)',t(-N), t(N)


!OPEN(UNIT=390,file='correlation_v04.txt',status='unknown')
!WRITE(390,'(5(1x,a12),1(1x,a6))') 't(j)', 'lambda(j)', 'g(j)', 'h(j)', 'y(j)', 'j'
!    DO j=-N,N
!        WRITE(390,'(5(1x,e12.5),1(1x,i6))') t(j), lambda(j), g(j), h(j), y(j), j
!    END DO
!CLOSE(UNIT=390)



!OPEN(UNIT=390,file='f.txt',status='unknown')
!    DO j=jmin,jmax
!        DO k=-N,N-1
!!            WRITE(390,'(2(1x,e12.5),1(1x,e12.5),1(1x,i6))') t(j), lambda(k), f(j,k)
!        END DO
!    END DO
!CLOSE(UNIT=390)

!STOP
